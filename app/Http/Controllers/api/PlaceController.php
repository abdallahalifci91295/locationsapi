<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlaceResource;
use Illuminate\Support\Facades\Validator;
use App\Place;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PlaceController extends Controller
{
	use ApiResponseTrait;
     Public function getNearestTo(Request $request)
   {
	   // to validate location variable sent if it is empty or not valid format
		$validate=Validator::make($request->all(),[
		'location'=> array('required','regex: ^[-+]?[0-9]*\.?[0-9]?+\,[-+]?[0-9]*\.?[0-9]?^'),
		
		]);
		if ($validate->fails()){
			return  $this-> apiResponse (null,$validate->errors(),400);
		}
		
		//extract latitude and longitude from location variable	
		$splitLocation = explode(',', $request->location, 2); 
		 if(!empty($splitLocation)){
		$latitude = $splitLocation[0];
		$longitude = $splitLocation[1];
		 }
  
		//check ranges of latitude and longitude
		if ($latitude < -90.00 ||$latitude>90.00||$longitude<-180.00||$longitude>180.00){
			return  $this-> apiResponse (null,'invalid latitude or longitude ranges',400);
		}
		
        //compute the distance to sort locations by nearest 
		$nearest= Place::select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( lat ) ) ) ) AS distance'))
		->orderBy('distance')-> paginate($this->paginateNumber);
	   
		return  $this-> apiResponse ($nearest);
		
	   
   }
   
   
}
