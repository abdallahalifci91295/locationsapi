<?php

namespace App\Http\Controllers\Api; //namespace for the api controller
use Illuminate\Support\Facades\Validator;
use Grimzy\LaravelMysqlSpatial\Types\Point;

Trait ApiResponseTrait
{

    public $paginateNumber = 5;

    /*
     * the common response shape
     * [
        'date'=>'returned data',
        'status'=> true or false,
        'error'=> 'error message',
        '
    ]
    */
	
// function called when aim to respond with api
    public function apiResponse($data = null, $error = null, $code = 200)
    {
        $array = [
            'data' => $data,
            'status' => in_array($code, $this->successCode()) ? true : false,
            'error' => $error,
        ];

        return response($array, $code);
    }

    public function successCode()
    {
        return [200, 201, 202];
    }






}