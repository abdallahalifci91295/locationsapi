<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
          'key'=>$this->id,
          'longitude'=>$this->lng,
          'latitude'=>$this->lat,
		  'name'=>$this->name,
        ];
    }
}
