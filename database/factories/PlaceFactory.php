<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Place::class, function (Faker $faker) {
    return [
        'lng' => $faker->randomFloat($nbMaxDecimals = NULL, $min = -180, $max = 180) ,
        'lat' => $faker->randomFloat($nbMaxDecimals = NULL, $min = -90, $max = 90) ,
		'name' =>$faker->name

    ];
});
